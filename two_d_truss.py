#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   two_d_truss.py

@author Till Junge <till.junge@epfl.ch>

@date   19 Mar 2019

@brief  Simple 2d truss calculator

Copyright © 2019 Till Junge

two_d_truss.py is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

two_d_truss.py is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with two_d_truss.py; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""
import matplotlib.pyplot as plt
import numpy as np

from matplotlib import rc
rc('font',**{'family':'serif'})
rc('text', usetex=False)
import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 144

from matplotlib.patches import Polygon

def evaluate_truss(joints, struts, reactions, external_loads):
    m = len(struts)    # nb of members
    r = len(reactions) # nb of reactions
    j = len(joints)    # nb of joints
    n = m + r # nb of unknowns


    if (n % 2):
        raise Exception("odd number of unknowns! In 2d, there has to be an "
                        "even number of unknowns.")
    if (n > 2*j):
        raise Exception(
            ("too many unknowns! You do not have enough joints. There are {} "
             " members and {} reactions, but only {} joints").format(
                 m, r, j))
    elif (n < 2*j):
        raise Exception("too many joints! Your truss is hyperstatic.")

    # set up system of equations A·x=b, where A is the system matrix,
    # b the external loads and x the vector of reactions and internal
    # loads
    A = np.zeros((n, n))
    for dof_id, strut in enumerate(struts):
        i, j = strut
        vector = joints[j] - joints[i]
        direction = vector/ np.linalg.norm(vector)
        A[2*i  , dof_id] += direction[0]
        A[2*i+1, dof_id] += direction[1]
        A[2*j  , dof_id] -= direction[0]
        A[2*j+1, dof_id] -= direction[1]
        pass

    for dof_id, reaction in enumerate(reactions, start=m):
        joint_id, direction = reaction
        direction = direction/np.linalg.norm(direction)
        A[2*joint_id  , dof_id] += direction[0]
        A[2*joint_id+1, dof_id] += direction[1]
        pass

    b = np.zeros(n)
    for load in external_loads:
        joint_id, load_vector = load
        b[2*joint_id  ] = -load_vector[0]
        b[2*joint_id+1] = -load_vector[1]
        pass

    x = np.linalg.solve(A, b)
    return x, A, b


def brace(text, format_str='{}'):
    return '{' + format_str.format(text) + '}'

def plot_truss(joints, struts, reactions=None, external_loads=None,
                   with_solution=False, scale=1, border=1/6):
    """plots a 2d truss defined by the joint positions and the connecting struts.
    Reactions and external loads are indicated by red and blue arrows,
    respectively. Optionally, the function also evaluates the internal loads and
    plots them in form of a N/L diagram. The size of the plot can be influenced
    through the scale parameter, and the size of the free boarder and length of
    reaction/load vectors by the border parameter.

    Keyword Arguments:
    joints         -- coordinates of the joints in a n×2 matrix
    struts         -- a matrix of shape j×2 where each row contains the indices
                      of the two joints it links
    reactions      -- list of tuples containing the joint index and the
                      direction of a reaction
    external_loads -- list of tuples containing the joint index and the force
                      vector
    with_solution  -- (default False) if True, the internal loads are computed
                      and plotted
    scale          -- (default 1) scales the size of the plot
    border         -- (default 1/6) scales the size of the reaction vectors,
                      force vectors and load diagrams

    """
    if reactions is None:
        reactions = list()
    if external_loads is None:
        external_loads = list()

    fig = plt.figure(figsize=[scale * s for s in
                              mpl.rcParams['figure.figsize']])
    ax = fig.add_subplot(111, aspect='equal')
    for strut in struts:
        i, j = strut
        ax.plot((joints[i, 0], joints[j, 0]), (joints[i, 1], joints[j, 1]),
                "ko-")
        pass

    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()

    struct_len= max(xmax - xmin, ymax - ymin)
    ax.set_xlim([xmin-struct_len*2*border, xmax+struct_len*2*border])
    ax.set_ylim([ymin-struct_len*2*border, ymax+struct_len*2*border])
    scale = struct_len*border
    solutions = None
    if with_solution:
        solutions, A, b = evaluate_truss(joints, struts, reactions,
                                             external_loads)
        max_load = abs(solutions).max()
        load_normalization = scale/max_load
        pass

    for count, strut in enumerate(struts):
        i, j = strut
        x, y = .5*(joints[i] + joints[j])
        direction = (joints[i] - joints[j]) / np.linalg.norm(joints[i] -
                                                             joints[j])
        dx, dy = (direction*scale)
        ax.plot((joints[i, 0], joints[j, 0]), (joints[i, 1], joints[j, 1]),
                "ko-")
        if solutions is None:
            label = "$N_{}$".format(brace(count))
        else:
            label = "$N_{} = {}$".format(brace(count), round(solutions[count],
                                                             2))
            ax.text(x, y, label, ha='center', va='center',
                    bbox=dict(boxstyle="round",
                              alpha=.8,
                              fc='white',
                              ec='none'))
            normal = (np.array((-direction[1], direction[0])) *
                      solutions[count] * load_normalization)
            fill_color = 'b' if solutions[count] > 0 else 'r'
            p = Polygon((joints[i],
                         joints[j],
                         joints[j] + normal,
                         joints[i] + normal),
                        fc=fill_color,
                        alpha=.5)
            ax.add_patch(p)
            pass
        pass


    centroid = joints.mean(axis=0)
    for count, reaction in enumerate(reactions):
        joint_id, direction = reaction
        x_end, y_end = joints[joint_id]
        dx, dy = direction*scale
        x, y = x_end - 1.2*dx, y_end - 1.2*dy
        xlabel, ylabel = x, y

        from_centroid = joints[joint_id] - centroid
        if np.dot(from_centroid, np.array([dx, dy])) > 0:
            x, y = x_end + .2*dx, y_end+ .2*dy
            xlabel, ylabel = x+dx, y+dy
        ax.arrow(x, y, dx, dy, color='r', head_width=scale/5,
                 length_includes_head=True)
        if solutions is None:
            label = "$R_{}$".format(count)
        else:
            label = "$R_{} = {}$".format(
                count, round(solutions[count + len(struts)], 2))
        ax.text(xlabel-dy*.3, ylabel+dx*.3,
                label, color='r', ha='center', va='center',
                bbox=dict(boxstyle="round",
                          alpha=.8,
                          fc='white',
                          ec='none'))
        pass

    for count, load in enumerate(external_loads, start=1):
        joint_id, load_vector = load
        direction = load_vector/np.linalg.norm(load_vector)
        x_end, y_end = joints[joint_id]
        dx, dy = direction*scale
        x, y = x_end - 1.2*dx, y_end - 1.2*dy
        xlabel, ylabel = x, y

        from_centroid = joints[joint_id] - centroid
        if np.dot(from_centroid, np.array([dx, dy])) > 0:
            x, y = x_end + .2*dx, y_end+ .2*dy
            xlabel, ylabel = x+dx, y+dy

        ax.arrow(x, y, dx, dy, color='b', head_width=scale/5,
                 length_includes_head=True)
        ax.text(xlabel-dy*.3, ylabel+dx*.3,
                "$F_{}$".format(count), color='b', ha='center', va='center',
                bbox=dict(boxstyle="round",
                          alpha=.8,
                          fc='white',
                          ec='none'))
        pass

    fig = plt.gcf()

    if solutions is None:
        return fig
    else:
        return fig, A, b
